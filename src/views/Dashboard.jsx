import React from 'react';
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types';
import NewPost from '../components/NewPost'
import PostListItem from '../components/PostListItem'
import withStyles from '@material-ui/core/styles/withStyles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import http from '../utils/AxiosService'

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  buttonNewPost: {
    float: 'right',
    marginTop: '-42px',
    marginRight: '20px'
  },
  noPost: {
    marginLeft: 'auto',
    marginRight: 'auto', 
    maxWidth: '500px',
    fontSize: '22px',
    boxShadow: '2px 2px 2px 2px rgba(0, 0, 0, 0.2)',
    borderRadius: '4px',
    marginTop: '60px',
    padding: '40px'
  }
});

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Dashboard extends React.Component {
  state = {
    value: 0,
    open: false,
    posts: []
  };

  handleChange = (event, value) => {
    this.setState({
      value,
    }, this.fetchPosts);
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  changePosts = () => {
    this.fetchPosts()
  }

  fetchPosts = () => {
    var endpoint = '/posts'
    switch (this.state.value) {
      case 0 :
        endpoint += '/subscriptions/' + JSON.parse(localStorage.getItem('user')).id
        break
      case 1 :
        endpoint += '/interests/' + JSON.parse(localStorage.getItem('user')).id
        break
      case 2 :
        endpoint += '/trending/'
        break
      default :
        break
    }
    const url = process.env.REACT_APP_BACKEND_URL + endpoint;
      http.get(url)
        .then(response => {
          if(response.status === 200){
            this.setState({
              posts: response.data
            })
          }
          else{
            alert('Failed to fetch posts')
          }
        })
  }

  componentDidMount() {
    this.fetchPosts()
  }
  
  render() {
    const { classes } = this.props;
    const { value, open, posts } = this.state;

    return (
      <main className={classes.main}>
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}
          >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.flex}>
                New post
              </Typography>
            </Toolbar>
          </AppBar>
          <NewPost closeFunc={this.handleClose.bind(this)} />
        </Dialog>
        <AppBar position="static">
          <Tabs value={value} onChange={this.handleChange}>
            <Tab label="Subscriptions" />
            <Tab label="Your interests" />
            <Tab label="Trending" />
          </Tabs>
        </AppBar>
        <Button className={classes.buttonNewPost} variant="contained" color="secondary" onClick={this.handleClickOpen}>
          New post
        </Button>
        <TabContainer>
          { 
            posts.length === 0 ? 
            <div className={classes.noPost}>Oops! Looks like you haven't follow any topic yet. Look for something you're interested in in the search bar above and click on the follow button to explore the content you like !</div> : 
            posts.map((post, index) => (<PostListItem closeFunc={this.handleClose.bind(this)} key={index} post={post} />))
          }
        </TabContainer>
      </main>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(withRouter(Dashboard)));