import React, { useState } from 'react';
import { Link, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import http from '../utils/AxiosService'

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  account : {
    marginTop: theme.spacing.unit * 2
  }
});

function SignIn(props) {
  const { classes } = props

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  function handleSubmit(event) {
    event.preventDefault()

    const url = process.env.REACT_APP_BACKEND_URL + '/authentication/signin';
    http.post(url, {
      email,
      password
    })
    .then(response => {
      console.log(response.status)
      const user = {
        jwt: response.data.token,
        id: response.data.user.id,
        firstName: response.data.user.firstName,
        lastName: response.data.user.lastName,
        email: response.data.user.email,
        avatarUrl: response.data.user.avatarUrl
      };

      // Store the user in local storage
      localStorage.setItem('user', JSON.stringify(user));

      // Go to the next URL when logged
      props.history.push('/');
    })
  }

  return (
    <main className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit} >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">Email Address</InputLabel>
            <Input id="email" value={email} onChange={e => setEmail(e.target.value)} name="email" autoComplete="email" autoFocus />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input name="password" value={password} onChange={e => setPassword(e.target.value)} type="password" id="password" autoComplete="current-password" />
          </FormControl>
          <Button
            type="submit"
            value="Submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign in
          </Button>
        </form>
        <div className={classes.account}>
          Doesn't have an account yet ? <Link to="/signup">Register</Link> !
        </div>
        
      </Paper>
    </main>
  )
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(withRouter(SignIn)));