import React, { useState } from 'react';
import { Link, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import http from '../utils/AxiosService'



const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    account : {
        marginTop: theme.spacing.unit * 2
    }
});

function SignUp(props) {

    const { classes } = props;
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConf, setPasswordConf] = useState('');

    function handleSubmit(event) {
        event.preventDefault()
        if(password !== passwordConf){
            alert("verify password")
        }
        else {
            const url = process.env.REACT_APP_BACKEND_URL + '/authentication/signup';
            http.post( url , {
                firstName,
                lastName,
                email,
                password
              })
              .then(response => {
                  if(response.status === 200){
                    props.history.push('/signin');
                  }
                  else{
                      alert('Failed to create user !')
                  }
              })
        }
    }


    return (
        <main className={classes.main}>
            <CssBaseline />
            <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockIcon />
                </Avatar>
                <Typography component="h1" variant="h5"> Sign Up </Typography>
                <form className={classes.form} onSubmit={handleSubmit} >
                    <FormControl margin="normal" required fullWidth>    
                        <InputLabel>First Name</InputLabel>
                        <Input id="firstName" value={firstName} onChange={e => setFirstName(e.target.value)} name="firstName" autoFocus/>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel>Last Name</InputLabel>
                        <Input id="lastName" value={lastName} onChange={e => setLastName(e.target.value)} name="lastName"/>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">Email Address</InputLabel>
                        <Input id="email" value={email} onChange={e => setEmail(e.target.value)} name="email" autoComplete="email"/>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Input name="password" value={password} onChange={e => setPassword(e.target.value)} type="password" id="password"/>
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Confirm Password</InputLabel>
                        <Input name="passwordConf" value={passwordConf} onChange={e => setPasswordConf(e.target.value)} type="password" id="passwordConf"/>
                    </FormControl>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign up
                    </Button>
                </form>
                <div className={classes.account}>
                    Already have an account ? <Link to="/signin">Login</Link> !
                </div>
            </Paper>
        </main>
    );
}

SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(withRouter(SignUp)));