import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import PersonIcon from '@material-ui/icons/Person';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import Dialog from '../components/ProfileInfoDialog'
import Delete from '../components/DeleteAccount'
import http from '../utils/AxiosService'


const styles = theme => ({
  main: {
      width: `auto`,
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      alignItems: 'center',
      [theme.breakpoints.up(800 + theme.spacing.unit * 3 * 2)]: {
          width: `${theme.spacing.unit * 100}px`,
          marginLeft: 'auto',
          marginRight: 'auto',
      },
  },
  paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
      width : `100%`
  },
  avatar: {
    margin: 10,
    width: '25%',
    height: '525%',
  },
  row: {
    display: 'flex',
    width: '100%',
    textAlign: 'center',
    marginBottom : `${theme.spacing.unit * 2}px`
  },
  column: {
    flex: '50%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  icon: {
    position: 'relative',
    top: 5,
  },
});

const defaultAvatarUrl = window.location.origin + '/img/avatar.png';

class Profile extends Component {

  constructor(props){
    super(props);
    const user = JSON.parse(localStorage.getItem('user'));
    this.state = {
      jwt : user.jwt,
      id : user.id,
      firstName : user.firstName,
      lastName : user.lastName,
      email : user.email,
      avatarUrl : user.avatarUrl
    }
  }

  componentDidMount(){
    if(this.state.avatarUrl === 'default') {
      this.setState({ avatarUrl: defaultAvatarUrl })
    }

    const urlFollowing = process.env.REACT_APP_BACKEND_URL + '/users/following/' + this.state.id;
    const urlFollowers = process.env.REACT_APP_BACKEND_URL + '/users/follower/' + this.state.id;

    http.get(urlFollowing).then(res => {
      this.setState({ following : res.data.count});
    });

    http.get(urlFollowers).then(res => {
      this.setState({ followers : res.data.count});
    });

  }

  updateUser = (user) => {
    const url = process.env.REACT_APP_BACKEND_URL + '/users';
    let { firstName, lastName, email, avatarUrl } = user
    if(avatarUrl === ''){
      avatarUrl = defaultAvatarUrl;
    }
    http.put(url, {
      id : this.state.id,
      firstName,
      lastName,
      email,
      avatarUrl
    })
    .then(res => {
      if(res.status === 200){
        this.setState({
          jwt: this.state.jwt,
          id: this.state.id,
          firstName: user.firstName,
          lastName: lastName,
          email: email,
          avatarUrl : avatarUrl
        });
        localStorage.setItem('user', JSON.stringify(this.state));
      }
      else{
          alert('Failed to update user !')
      }
    })
  }



  render() {
    const { classes } = this.props;

    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar alt="avatar" src={this.state.avatarUrl} className={classes.avatar} />
          <div>
            <h1> {this.state.firstName} {this.state.lastName} </h1>
          </div>
          <div className={classes.row}>
            <div className={classes.column}>
              <strong>Followers</strong>
              <span>{this.state.followers} <PersonIcon className={classes.icon}/></span>
            </div>
            <div className={classes.column}>
              <strong>Following</strong>
              <span>{this.state.following} <PersonIcon className={classes.icon}/></span>
            </div>
          </div>
          <div className={classes.row}>
            <div className={classes.column}>
              <Dialog updateUser={this.updateUser} state={this.state}/>
            </div>
            <div className={classes.column}>
              <Delete/>
            </div>
          </div>

        </Paper>
      </main>
    )
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Profile)