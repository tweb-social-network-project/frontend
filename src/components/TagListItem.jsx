import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import withStyles from '@material-ui/core/styles/withStyles';
import { withRouter} from 'react-router-dom'
import http from '../utils/AxiosService'

const styles = theme => ({
  actions: {
    display: 'flex',
  },
  buttonFollow: {
    width: '100px',
    fontSize: '11px'
  }
})

class TagListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleClickFollow = () => {
    const url = process.env.REACT_APP_BACKEND_URL + '/interests';
    http.post(url, { sourceId: JSON.parse(localStorage.getItem('user')).id, targetId: this.props.tag.id, properties: null })
        .then(response => {
            if(response.status !== 200){
              alert('Failed to follow tag !')
            }
        })
  }

  render() {
    const { tag, classes } = this.props
    return (
      <ListItem className="list-item" alignItems="flex-start">
        <ListItemText
          primary={tag.name}
        />
        <Button variant="contained" color="secondary" className={classes.buttonFollow} onClick={this.handleClickFollow}>Follow</Button>
      </ListItem>
    );
  }
}

TagListItem.propTypes = {
  classes: PropTypes.object.isRequired,
  tag: PropTypes.object.isRequired
};

export default (withStyles(styles)(withRouter(TagListItem)));
