import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Divider from '@material-ui/core/Divider';
import Menu from '@material-ui/core/Menu';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import TagListItem from './TagListItem'
import UserListItem from './UserListItem'
import PostSearchListItem from './PostSearchListItem'
import debounce from 'debounce'
import http from '../utils/AxiosService'

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
    marginLeft: theme.spacing.unit 
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  button: {
    margin: theme.spacing.unit,
    color: 'white'
  },
  input: {
    display: 'none',
  },
  dashBoardMenuItem: {
    color: 'white',
    textDecoration: 'none',
    float: 'left',
    marginLeft: '30px'
  },
  suggestions: {
    backgroundColor: 'white',
    position: 'absolute',
    maxWidth: '500px',
    top: '50px',
    left: '190px',
    borderBottomLeftRadius: '5px',
    borderBottomRightRadius: '5px',
    borderTopRightRadius: '5px',
    zIndex: '1000',
    boxShadow: '2px 2px 2px 2px rgba(0, 0, 0, 0.2)',
  }
});

class Header extends Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    user: null,
    tags: [],
    users: [],
    posts: [],
    suggestionOpen: false
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  handleLogout = () => {
    localStorage.removeItem('user');
    this.props.history.push('/');
    this.handleMenuClose()
  }

  handleProfileButtonClick = () => {
    this.props.history.push('/profile');
    this.handleMenuClose()
  }

  handleSearchChange = debounce((value) => {
    if (value !== '') {
      this.fetchTags(value)
      this.fetchUsers(value)
      this.fetchPosts(value)
    }
  }, 300)

  fetchTags = (value) => {
    http.get(process.env.REACT_APP_BACKEND_URL + '/tags/search/' + value)
        .then(response => {
          this.setState({
            tags: response.data
          }, this.showSuggestionsList)
        })
  }

  fetchUsers = (value) => {
    http.get(process.env.REACT_APP_BACKEND_URL + '/users/search/' + value)
        .then(response => {
          this.setState({
            users: response.data
          }, this.showSuggestionsList)
        })
  }

  fetchPosts = (value) => {
    http.get(process.env.REACT_APP_BACKEND_URL + '/posts/search/' + value)
        .then(response => {
          this.setState({
            posts: response.data
          }, this.showSuggestionsList)
        })
  }

  showSuggestionsList = () => {
    this.setState({
      suggestionOpen: true
    })
  }

  hideSuggestionsList = debounce(() => {
    this.setState({
      suggestionOpen: false
    })
  }, 100)

  render() {
    const { anchorEl, mobileMoreAnchorEl, tags, users, posts, suggestionOpen } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const authMenu = (
      <React.Fragment>
        <Link to="/dashboard" className={classes.dashBoardMenuItem}>Newsfeed</Link>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            onChange={e => this.handleSearchChange(e.target.value)}
            onBlur={this.hideSuggestionsList}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
          />
        </div>
          { suggestionOpen ? 
            <div className={classes.suggestions}>
              <List subheader={<ListSubheader>Tags</ListSubheader>} className={classes.root}>
                <Divider />
                { tags.map((tag, index) => (<TagListItem key={index} tag={tag} />)) }
              </List>
              <List subheader={<ListSubheader>Users</ListSubheader>} className={classes.root}>
                <Divider />
                { users.map((user, index) => (<UserListItem key={index} user={user} />)) }
              </List>
              <List subheader={<ListSubheader>Posts</ListSubheader>} className={classes.root}>
                <Divider />
                { posts.map((post, index) => (<PostSearchListItem key={index} post={post} />))}
              </List>
            </div>
            : <div></div> 
          }
        <div className={classes.grow} />
        <div className={classes.sectionDesktop}>
          <IconButton
            aria-owns={isMenuOpen ? 'material-appbar' : undefined}
            aria-haspopup="true"
            onClick={this.handleProfileMenuOpen}
            color="inherit"
          >
            <AccountCircle></AccountCircle>
          </IconButton>
        </div>
        <div className={classes.sectionMobile}>
          <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
            <MoreIcon />
          </IconButton>
        </div>
      </React.Fragment>
    )
    
    const nonAuthMenu = (
      <React.Fragment>
        <div className={classes.grow} />
        <Link to="/signup" style={{ textDecoration: "none" }}>
          <Button className={classes.button}>
            Sign up
          </Button>
        </Link>
        <Link to="/signin" style={{ textDecoration: "none" }}>
          <Button className={classes.button}>
            Sign in
          </Button>
        </Link>
      </React.Fragment>
    )

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem onClick={this.handleProfileButtonClick}>My profile</MenuItem>
        <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
      </Menu>
    );

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton color="inherit">
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    );

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <img src={require('../assets/images/bidon.png')} width="35" height="25" alt=""/>
            <Link to="/" style={{ textDecoration: "none", color: 'white' }}>
              <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                Agora
              </Typography>
            </Link>
            {localStorage.getItem('user') === null ? nonAuthMenu : authMenu}
          </Toolbar>
        </AppBar>
        {renderMenu}
        {renderMobileMenu}
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(withRouter(Header)));