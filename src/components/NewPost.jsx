import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';
import {Editor, EditorState, RichUtils, convertToRaw} from 'draft-js';
import { withRouter} from 'react-router-dom'
import http from '../utils/AxiosService'

const styles = theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  textEditButton: {
    margin: theme.spacing.unit,
    width: '10px'
  },
  editor: {
    height: '300px',
    overflow: 'auto',
    borderStyle: 'solid',
    borderWidth: '0.5px',
    borderColor: 'gray',
    cursor: 'text',
    marginBottom: '10px'
  },
  newPostDiv: {
    padding: '30px'
  }
})

class NewPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      title: '',
      content: ''
    };
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'))
    const url = process.env.REACT_APP_BACKEND_URL + '/posts/' + user.id;
    http.post( url , {
        title: this.state.title,
        content: this.state.content,
        token: user.jwt
      })
      .then(response => {
          if(response.status !== 200) {
              alert('Failed to create user !')
          }
      })
  }

  setTitle = (title) => {
    this.setState({
      title
    })
  }

  onContentChange = (editorState) => {
    this.setState({
      editorState
    })
  }

  handleKeyCommand = (command) => {
    const newState = RichUtils.handleKeyCommand(this.state.editorState, command)
    if (newState) {
        this.onContentChange(newState);
        return 'handled';
    }
    return 'not-handled';
  }
  
  onUnderlineClick = () => {
    this.onContentChange(RichUtils.toggleInlineStyle(this.state.editorState, 'UNDERLINE'))
  }

  onBoldClick = () => {
    this.onContentChange(RichUtils.toggleInlineStyle(this.state.editorState, 'BOLD'))
  }

  onItalicClick = () => {
    this.onContentChange(RichUtils.toggleInlineStyle(this.state.editorState, 'ITALIC'))
  }

  focus() {
    this.editor.focus();
  }

  save = () => {
    const newContent = JSON.stringify(convertToRaw(this.state.editorState.getCurrentContent()))
    const url = process.env.REACT_APP_BACKEND_URL + '/posts';
    const linkedPostId = this.props.linkedPost ? this.props.linkedPost.id : undefined
    http.post(url, {
        post: {
          title: this.state.title,
          content: newContent,
        },
        authorId: JSON.parse(localStorage.getItem('user')).id,
        replyToId: linkedPostId
      })
      .then(response => {
        if(response.status !== 200){
          alert('Failed')
        }
        this.props.closeFunc()
      })
  }

  render() {
    const { classes } = this.props;
    const { title } = this.state
    return (
      <div className={classes.newPostDiv}>
        <form className={classes.form} onSubmit={this.handleSubmit} >
          <FormControl margin="normal" required fullWidth>    
            <InputLabel>Title</InputLabel>
            <Input id="title" value={title} onChange={e => this.setTitle(e.target.value)} name="title" autoFocus/>
          </FormControl>
          <Button className={classes.textEditButton} onClick={this.onUnderlineClick}>U</Button>
          <Button className={classes.textEditButton} onClick={this.onBoldClick}><b>B</b></Button>
          <Button className={classes.textEditButton} onClick={this.onItalicClick}><em>I</em></Button>        
          <div onClick={this.focus.bind(this)} className={classes.editor}>
            <Editor
              ref={node => this.editor = node}
              editorState={this.state.editorState}
              onChange={this.onContentChange}
              handleKeyCommand={this.handleKeyCommand}
            />
          </div>
          <Button
              type="button"
              variant="contained"
              color="primary"
              onClick={this.save}
          >
              Save
          </Button>
        </form>
      </div>
    );
  }
}

NewPost.propTypes = {
  classes: PropTypes.object.isRequired,
  linkedPost: PropTypes.object,
  closeFunc: PropTypes.func.isRequired
};

export default (withStyles(styles)(withRouter(NewPost)));
