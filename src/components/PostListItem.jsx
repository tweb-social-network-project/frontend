import React from 'react';
import PropTypes from 'prop-types';
import NewPost from '../components/NewPost'
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import {convertFromRaw} from 'draft-js';
import { stateToHTML } from "draft-js-export-html";
import Slide from '@material-ui/core/Slide';
import { Markup } from 'interweave';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import withStyles from '@material-ui/core/styles/withStyles';
import { withRouter} from 'react-router-dom'
import http from '../utils/AxiosService'

const styles = theme => ({
  PostListItem: {
    marginLeft: 'auto',
    marginRight: 'auto', 
    maxWidth: '1000px',
    boxShadow: '2px 2px 2px 2px rgba(0, 0, 0, 0.2)',
    borderRadius: '4px',
    marginBottom: '20px',
    backgroundColor: 'white'
  },
  actions: {
    display: 'flex',
  }
})

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class PostListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleClickFollow = () => {
    const url = process.env.REACT_APP_BACKEND_URL + '/interests';
    http.post(url, { sourceId: JSON.parse(localStorage.getItem('user')).id, targetId: this.props.post.author.id, properties: null })
        .then(response => {
            if(response.status !== 200){
              alert('Failed to follow user !')
            }
        })
  }

  render() {
    const { classes, post } = this.props
    const { open } = this.state
    return (
      <ListItem className={classes.PostListItem} alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt={post.author.firstName} src={post.author.avatar} />
        </ListItemAvatar>
        <ListItemText
          disableTypography
          primary={post.post.title}
          secondary={
            <React.Fragment>
              <Typography component="span" color="textPrimary">
                {`${post.author.firstName} ${post.author.lastName}`}
              </Typography>
              <Markup content={stateToHTML(convertFromRaw(JSON.parse(post.post.content)))} />
            </React.Fragment>
          }
        />
        <div className={classes.actions}>
          <Button onClick={this.handleClickOpen}>Comment</Button>
          <Button onClick={this.handleClickFollow}>Follow</Button>
          <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}
          >
            <AppBar>
              <Toolbar>
                <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                  <CloseIcon />
                </IconButton>
                Comment
              </Toolbar>
            </AppBar>
            <NewPost closeFunc={this.handleClose.bind(this)} linkedPost={post.post} />
          </Dialog>
        </div>
      </ListItem>
    );
  }
}

PostListItem.propTypes = {
  classes: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
};

export default (withStyles(styles)(withRouter(PostListItem)));
