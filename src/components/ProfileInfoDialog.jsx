import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';



const styles = theme =>({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  }
});

class ProfileInfoDialog extends Component {
  constructor(props){
    super(props);
    this.state = {
      open : false,
      firstName : props.state.firstName,
      lastName : props.state.lastName,
      email : props.state.email,
      avatarUrl : props.state.avatarUrl
    }
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  handleClickOpen = () => {
    this.setState({open : true});
  }

  handleClose = () => {
    this.setState({open : false});
  }

  handleSave = () => {
    const { firstName, lastName, email, avatarUrl } = this.state;
    this.props.updateUser({ firstName, lastName, email, avatarUrl });
    this.setState({open : false});
  }

  handleChange = name => event => {
    this.setState({ [name] : event.target.value });
  };

  render(){
    const open = this.state.open;
    const { classes } = this.props;
    
    return (
      <div>
        <Button variant="outlined" onClick={this.handleClickOpen}>Modify user info</Button>
        <Dialog open={open} 
          onClose={this.handleClose} 
          aria-labelledby="form-dialog-title"
          TransitionComponent={this.Transition}
        >
          <DialogTitle id="form-dialog-title">Modify personnal informations</DialogTitle>
          <DialogContent>
          <form className={classes.container} noValidate autoComplete="off">
            <TextField
              id="firstName"
              label="Name"
              className={classes.textField}
              value={this.state.firstName}
              onChange={this.handleChange('firstName')}
              margin="normal"
              variant="outlined"
            />
            <TextField
              id="lastName"
              label="Last name"
              className={classes.textField}
              value={this.state.lastName}
              onChange={this.handleChange('lastName')}
              margin="normal"
              variant="outlined"
            />
            <TextField
              id="email"
              label="Email"
              className={classes.textField}
              value={this.state.email}
              onChange={this.handleChange('email')}
              margin="normal"
              variant="outlined"
            />
            <TextField
              id="avatarUrl"
              label="Avatar URL"
              className={classes.textField}
              value={this.state.avatarUrl}
              onChange={this.handleChange('avatarUrl')}
              margin="normal"
              variant="outlined"
            />
          </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSave} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ProfileInfoDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProfileInfoDialog);