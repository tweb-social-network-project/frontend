import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch , Redirect} from 'react-router-dom';
import Header from './components/Header'
import SignIn from './views/SignIn';
import SignUp from './views/SignUp';
import Dashboard from './views/Dashboard';
import Home from './views/Home';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Profile from './views/Profile';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      main: '#32746D'
    },
    secondary: {
      main: '#9EC5AB'
    },
    error: {
      main: '#545775'
    }
  },
});

function App() {
  return (
    <BrowserRouter>
      <div className='container'>
        <MuiThemeProvider theme={theme}>
          <Header />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/signin' component={SignIn} />
            <Route exact path='/signup' component={SignUp} />
            <PrivateRoute exact path='/profile' component={Profile} />
            <PrivateRoute exact path='/dashboard' component={Dashboard} />
            <Route render={function () {
              return <p>Page not found...</p>
            }} />
          </Switch>
        </MuiThemeProvider>
      </div>
    </BrowserRouter>
  );
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('user')
      ? <Component {...props} />
      : <Redirect to={{
          pathname: '/',
          state: { from: props.location }
        }} />
  )} />
)

export default App;
