# TWEB Social Network Project

## Frontend description

### User interface

The user interface is made with React, using [material-ui](https://material-ui.com/), a well known React UI framework.

The project is simple, there is a homepage for unlogged users, a login page and a register page.
When you are logged, you have access to your dashboard called "newsfeed", and a profile page.
On your newsfeed, you have three tabs :

- **Subscriptions**: this is where all the posts from users you are following.
- **Your interests**: this is where you find posts from every users, linked to the user's followed tags.
- **Trending**: this is where you can see the trending posts from every users.

You can also add a new post with the geogious button on the upper right !

The you have the profile page, where you can see all your informations, update it, and delete your account. There is a default image for avatar, but user can add his own by url.

Last but not least, in the header you can find the researsh bar where you can look for other users, tags, and posts.

### Technical specifications

#### Session management

When a user login, the server return users' informations and a JWT. All of this are stored in the local storage, and when the logout button is pressed, the local storage is cleared and you can no longer access protected users pages. Those pages are protected by the custom router, that verify if there is a user stored in the local storage.

#### Http communication

We use a custom Axios for http requests, we added the fact that every time axios is called, it will automatically add the JWT token stored in the local storage.

## Deployement

To have a working frontend in local, you must have your backend runnning in local too.

Then do :

`npm init`

`npm start`

and you can find everything running on [http://localhost:3000](http://localhost:3000)